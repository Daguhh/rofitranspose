RofiTranspose
=============

Display your entries line by line with rofi
 | A simple script to insert between a list of element and a rofi command

Afficher vos entrées ligne par ligne avec rofi
 | Un simple script à inserer entre la liste d'éléments et une commande rofi

Usage
-----

.. code:: bash

    echo <elements line by line> |
    ./rofi_transpose --columns <number of columns> |
    rofi -dmenu -columns <number of columns -lines <number of lines>

Example
-------

.. code:: bash

    printf " 1-1 \n 2-1 \n 3-1 \n 4-1 \n 1-2 \n 2-2 \n 3-2 \n 4-2 \n 1-3 \n 2-3 \n 3-3 \n 4-3" |
    rofi -dmenu -columns 3 -lines 4

.. code::

     1-1 | 1-2 | 1-3
     2-1 | 2-2 | 2-3
     3-1 | 3-2 | 3-3
     4-1 | 4-2 | 4-3

.. code:: bash

    printf " 1-1 \n 2-1 \n 3-1 \n 4-1 \n 1-2 \n 2-2 \n 3-2 \n 4-2 \n 1-3 \n 2-3 \n 3-3 \n 4-3" |
    ./rofi_transpose --columns 4 |
    rofi -dmenu -columns 4 -lines 3

.. code::

     1-1 | 2-1 | 3-1 | 4-1 
     1-2 | 2-2 | 3-2 | 4-2 
     1-3 | 2-3 | 3-3 | 4-3 

.. code:: bash

   printf " 1-1 ; 2-1 ; 3-1 ; 4-1 ; 1-2 ; 2-2 ; 3-2 ; 4-2 ; 1-3 ; 2-3" |
   ./rofi_transpose --columns 4 --sep_in ";" --sep_out ',' | 
   rofi -dmenu -columns 4 -lines 3 -sep ','

.. code::

     1-1 | 2-1 | 3-1 | 4-1 
     1-2 | 2-2 | 3-2 | 4-2 
     1-3 | 2-3 |     |     


Autodoc module
--------------

.. code::

   cd docs/
   python3 -m pip install -r requirements.txt
   make html


