=============
RofiTranspose
=============

.. toctree::
   :maxdepth: 2


.. automodule:: rofi_transpose
  :members:
  :member-order: bysource

